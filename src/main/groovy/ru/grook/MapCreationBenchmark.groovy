package ru.grook

import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(1)
class MapCreationBenchmark {
  int[] values;
  int total

  @Setup
  public void init() {
    Random r = new Random()
    total = 5;

    values = new int[total]
    for (int i=0; i< total; i++) {
      values[i] = r.nextInt();
    }
  }

  @Benchmark
  public void createGroovyMap(Blackhole bh) {
    Map m = [
      a: values[0],
      b: values[1],
      c: values[2],
      d: values[3],
      e: values[4],
    ]
    bh.consume(m);
  }

  @Benchmark
  public void createImplicitHashMap(Blackhole bh) {
    Map<String, Object> map = new HashMap<String, Object>(total);
    map.put("a",values[0])
    map.put("b",values[1])
    map.put("c",values[2])
    map.put("d",values[3])
    map.put("e",values[4])
    bh.consume(map);
  }
}
