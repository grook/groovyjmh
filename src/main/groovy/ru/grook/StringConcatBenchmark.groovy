package ru.grook

import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;
import java.io.StringWriter;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(1)
class StringConcatBenchmark {
  private Root root;

  @Setup
  public void prepareStrings() {
Random r = new Random()
    root = new Root();
    LeafTwo leafTwo = new LeafTwo()
    LeafOne leafOne = new LeafOne();

    leafOne.id = r.nextLong()
    leafTwo.id = r.nextLong()
    root.id = r.nextLong()
    root.leafTwo = leafTwo
    root.leafOne = leafOne
  }

  @Benchmark
  public String gString(Blackhole bh) {
    //as Object!
   bh.consume("${root.getLeafTwo().getId()}/${root.getLeafOne().getId()}/${root.getId()}/0.jpg");
 }

  @Benchmark
  public String gStringWithVariable(Blackhole bh) {
    //call toString
    String data = "${root.getLeafTwo().getId()}/${root.getLeafOne().getId()}/${root.getId()}/0.jpg";
   bh.consume(data);
 }
}
