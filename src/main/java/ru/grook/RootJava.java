package ru.grook;

public class RootJava {
  Long id;
  LeafOneJava leafOneJava;
  LeafTwoJava leafTwoJava;

  Long getId() { return id; }
  LeafOneJava getLeafOneJava() { return leafOneJava;}
  LeafTwoJava getLeafTwoJava() { return leafTwoJava; }
}
