package ru.grook;

import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;
import java.io.StringWriter;
import java.util.Random;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(1)
public class PropertyAccessBenchmarkJava {
  private RootJava rootJava;

  @Setup
  public void init() {
    Random r = new Random();

    rootJava = new RootJava();
    LeafOneJava leafOneJava = new LeafOneJava();
    LeafTwoJava leafTwoJava = new LeafTwoJava();
    leafOneJava.id = r.nextLong();
    leafTwoJava.id = r.nextLong();

    rootJava.id = r.nextLong();
    rootJava.leafOneJava = leafOneJava;
    rootJava.leafTwoJava = leafTwoJava;
  }

  @Benchmark
  public void accessJavaFields(Blackhole bh) {
    bh.consume(rootJava.id);
    bh.consume(rootJava.leafOneJava.id);
    bh.consume(rootJava.leafTwoJava.id);
  }

  @Benchmark
  public void accessJavaGetters(Blackhole bh) {
    bh.consume(rootJava.getId());
    bh.consume(rootJava.getLeafOneJava().getId());
    bh.consume(rootJava.getLeafTwoJava().getId());
  }
}
