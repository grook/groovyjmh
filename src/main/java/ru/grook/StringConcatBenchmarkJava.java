package ru.grook;

import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;
import java.io.StringWriter;
import java.util.Random;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(1)
public class StringConcatBenchmarkJava {
  private RootJava rootJava;

  @Setup
  public void prepareStrings() {
    Random r = new Random();

    rootJava = new RootJava();
    LeafOneJava leafOneJava = new LeafOneJava();
    LeafTwoJava leafTwoJava = new LeafTwoJava();
    leafOneJava.id = r.nextLong();
    leafTwoJava.id = r.nextLong();

    rootJava.id = r.nextLong();
    rootJava.leafOneJava = leafOneJava;
    rootJava.leafTwoJava = leafTwoJava;
  }

  @Benchmark
  public void stringBuilder(Blackhole bh) {
    bh.consume(new StringBuilder(rootJava.getLeafOneJava().getId().toString())
      .append("/").append(rootJava.getLeafTwoJava().getId()).append("/")
      .append(rootJava.getId()).append("/0.jpg")
      .toString());
  }

  @Benchmark
  public void stringBuilderWithVariable(Blackhole bh) {
    StringBuilder data = new StringBuilder(rootJava.getLeafOneJava().getId().toString())
      .append("/").append(rootJava.getLeafTwoJava().getId()).append("/")
      .append(rootJava.getId()).append("/0.jpg");
    bh.consume(data.toString());
  }

  @Benchmark
  public void stringBuffer(Blackhole bh) {
    bh.consume(new StringBuffer(rootJava.getLeafOneJava().getId().toString())
      .append("/").append(rootJava.getLeafTwoJava().getId()).append("/")
      .append(rootJava.getId()).append("/0.jpg")
      .toString());
  }

  @Benchmark
  public void stringBufferWithVariable(Blackhole bh) {
    StringBuffer data = new StringBuffer(rootJava.getLeafOneJava().getId().toString())
      .append("/").append(rootJava.getLeafTwoJava().getId()).append("/")
      .append(rootJava.getId()).append("/0.jpg");
    bh.consume(data.toString());
  }

  @Benchmark
  public void stringWriter(Blackhole bh) {
    StringWriter buffer = new StringWriter();
    buffer.write(rootJava.getLeafOneJava().getId().toString());
    buffer.write("/");
    buffer.write(rootJava.getLeafTwoJava().getId().toString());
    buffer.write("/");
    buffer.write(rootJava.getId().toString());
    buffer.write("/0.jpg");
    bh.consume(buffer.toString());
  }

  @Benchmark
  public void stringFormat(Blackhole bh) {
    bh.consume(String.format("%d/%d/%d/0.jpg",
      rootJava.getLeafOneJava().getId(),
      rootJava.getLeafTwoJava().getId(),
      rootJava.getId()));
  }

  @Benchmark
  public void stringPlus(Blackhole bh) {
    bh.consume(rootJava.getLeafOneJava().getId() + "/" + rootJava.getLeafTwoJava().getId() + "/" + rootJava.getId() + "/0.jpg");
  }
}
