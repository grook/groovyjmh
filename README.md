# Results #

## 13.03.15 ##

Benchmark                                           |Mode | Cnt | Score ± Error | Units
----------------------------------------------------|-----|-----|---------------|------
MapCreationBenchmark.createGroovyMap                |avgt |  10 | 0,147 ± 0,009 | us/op
MapCreationBenchmark.createImplicitHashMap          |avgt |  10 | 0,159 ± 0,007 | us/op
PropertyAccessBenchmark.accessGroovyFields          |avgt |  10 | 0,083 ± 0,001 | us/op
PropertyAccessBenchmark.accessGroovyGetters         |avgt |  10 | 0,045 ± 0,001 | us/op
PropertyAccessBenchmarkJava.accessJavaFields        |avgt |  10 | 0,008 ± 0,000 | us/op
PropertyAccessBenchmarkJava.accessJavaGetters       |avgt |  10 | 0,008 ± 0,000 | us/op
StringConcatBenchmark.gString                       |avgt |  10 | 0,048 ± 0,000 | us/op
StringConcatBenchmark.gStringWithVariable           |avgt |  10 | 0,744 ± 0,053 | us/op
StringConcatBenchmarkJava.stringBuffer              |avgt |  10 | 0,322 ± 0,009 | us/op
StringConcatBenchmarkJava.stringBufferWithVariable  |avgt |  10 | 0,327 ± 0,006 | us/op
StringConcatBenchmarkJava.stringBuilder             |avgt |  10 | 0,296 ± 0,012 | us/op
StringConcatBenchmarkJava.stringBuilderWithVariable |avgt |  10 | 0,296 ± 0,006 | us/op
StringConcatBenchmarkJava.stringFormat              |avgt |  10 | 2,096 ± 0,046 | us/op
StringConcatBenchmarkJava.stringPlus                |avgt |  10 | 0,302 ± 0,008 | us/op
StringConcatBenchmarkJava.stringWriter              |avgt |  10 | 0,332 ± 0,029 | us/op